// contracts/Token.sol
// SPDX-License-Identifier: Business Source License 1.1
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract TokenBurnable is ERC20, ERC20Burnable{
    constructor(uint256 initialSupply) ERC20("MyTokenBurnable", "MTKB") {
        _mint(msg.sender, initialSupply);
    }
}