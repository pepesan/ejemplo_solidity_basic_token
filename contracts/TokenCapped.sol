// contracts/Token.sol
// SPDX-License-Identifier: Business Source License 1.1
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Capped.sol";

contract TokenCapped is ERC20Capped{
    uint256 public initial;
    address private owner;
    constructor(uint256 initial_, uint256 cap) ERC20("MyTokenCapped", "MTKC") ERC20Capped(cap){
        initial = initial_;
        owner = msg.sender;
    }

    function issueToken() public{
        require(owner == msg.sender, "Need to be the owner");
        _mint(msg.sender, initial);
    }
    function mint(address destination, uint256 ammount) public{
        require(owner == msg.sender, "Need to be the owner");
        _mint(destination, ammount);
    }

}