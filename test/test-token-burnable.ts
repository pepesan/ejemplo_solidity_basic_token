import { BigNumber } from "ethers";

const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("MyTokenBurnable", function () {
  let initialSupply = '10000000000000000000000'; // 10000 * 1e18
  let Token;
  let hardhatToken;
  let owner;
  let addr1;
  let addr2;
  let addrs;

  beforeEach(async function () {
    // Get the ContractFactory and Signers here.
    Token = await ethers.getContractFactory("TokenBurnable");
    [this.owner, this.addr1, this.addr2, ...this.addrs] = await ethers.getSigners();
    this.hardhatToken = await Token.deploy(initialSupply);
    await this.hardhatToken.deployed();
  });
  it("Should return the correct Total Supply", async function () {
    const TokenAddress = this.hardhatToken.address;
    console.log("Token Address: " + TokenAddress);
    expect(initialSupply).to.equal(await this.hardhatToken.totalSupply());
  });
  it("The deployer account must have the initial supply", async function () {
    expect(initialSupply).to.equal(await this.hardhatToken.balanceOf(this.owner.address));
  });
  it("Another address must to have 0 balance", async function () {
    await this.hardhatToken.deployed();
    expect(0).to.equal(await this.hardhatToken.balanceOf(this.addr1.address));
  });
  it("Should checks ammounts after a transfer", async function () {
    const ownerBalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.owner.address);
    // Transfer 50 tokens from owner to addr1
    await this.hardhatToken.transfer(this.addr1.address, 50);
    const addr1BalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.addr1.address);
    expect(50).to.equal(addr1BalanceBeforeTransfer);

    const ownerBalanceAfterTransfer = await this.hardhatToken.balanceOf(this.owner.address);
    // se usa Bignumber porque TS no soporta por defecto cifras tan grandes
    expect(BigNumber.from("9999999999999999999950")).to.equal(ownerBalanceAfterTransfer);
  });
  it("Should transfer tokens between accounts", async function () {
    const ownerBalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.owner.address);
    // Transfer 50 tokens from owner to addr1
    await this.hardhatToken.transfer(this.addr1.address, 50);
    const addr1BalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.addr1.address);
    expect(50).to.equal(addr1BalanceBeforeTransfer);
    // Transfer 50 tokens from addr1 to addr2
    // We use .connect(signer) to send a transaction from another account
    await this.hardhatToken.connect(this.addr1).transfer(this.addr2.address, 50);
    const addr2BalanceAfterTransfer = await this.hardhatToken.balanceOf(this.addr2.address);
    const addr1BalanceAfterTransfer = await this.hardhatToken.balanceOf(this.addr1.address);
    expect(50).to.equal(addr2BalanceAfterTransfer);
    expect(0).to.equal(addr1BalanceAfterTransfer);
  });

  it("Should transfer tokens from one account to another with approve", async function () {
    const ownerBalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.owner.address);
    // Transfer 50 tokens from owner to addr1
    await this.hardhatToken.transfer(this.addr1.address, 50);
    const addr1BalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.addr1.address);
    // permitiendo gestionar desde la cuenta owner la cuenta 1
    await this.hardhatToken.connect(this.addr1).approve(this.owner.address, 50);
    const tokensallowed = await this.hardhatToken.allowance(this.addr1.address, this.owner.address);
    // console.log("Tokens Allowed: " + tokensallowed);
    expect(50).to.equal(tokensallowed);
    // Transfer 50 tokens from addr1 to addr2 with the Owner Account Permissions
    // We use .connect(signer) to send a transaction from another account
    await this.hardhatToken.transferFrom(this.addr1.address, this.addr2.address, 50);
    const addr2BalanceAfterTransfer = await this.hardhatToken.balanceOf(this.addr2.address);
    const addr1BalanceAfterTransfer = await this.hardhatToken.balanceOf(this.addr1.address);
    expect(50).to.equal(addr2BalanceAfterTransfer);
    expect(0).to.equal(addr1BalanceAfterTransfer);
  });
  it("Should burn tokens from owner", async function () {
    expect(initialSupply).to.equal(await this.hardhatToken.totalSupply());
    await this.hardhatToken.burn(50);
    const ownerBalanceAfterTransfer = await this.hardhatToken.balanceOf(this.owner.address);
    // se usa Bignumber porque TS no soporta por defecto cifras tan grandes
    expect(BigNumber.from("9999999999999999999950")).to.equal(ownerBalanceAfterTransfer);
  });
  it("Should burn tokens from owner another account", async function () {
    const ownerBalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.owner.address);
    // Transfer 50 tokens from owner to addr1
    await this.hardhatToken.transfer(this.addr1.address, 50);
    const addr1BalanceBeforeTransfer = await this.hardhatToken.balanceOf(this.addr1.address);
    // permitiendo gestionar desde la cuenta owner la cuenta 1
    await this.hardhatToken.connect(this.addr1).approve(this.owner.address, 50);
    const tokensallowed = await this.hardhatToken.allowance(this.addr1.address, this.owner.address);
    // console.log("Tokens Allowed: " + tokensallowed);
    expect(50).to.equal(tokensallowed);
    await this.hardhatToken.burnFrom(this.addr1.address, 50);
    const addr1BalanceAfterBurning = await this.hardhatToken.balanceOf(this.addr1.address);
    // Check if the token are burned
    expect(0).to.equal(addr1BalanceAfterBurning);
  });
});
